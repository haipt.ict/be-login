/**
 * View Models used by Spring MVC REST controllers.
 */
package com.demo.login.web.rest.vm;
